import React, { PureComponent } from "react";
import { View, StyleSheet } from "react-native";
import Header from '@src/components/ui/Header'
import ListCompanyFood from '@src/components/ui/ListCompanyFood'
import BottomBar from '@src/components/ui/BottomBar'
import HeaderListCompanyFood from '@src/components/ui/HeaderListCompanyFood'

class ListCompanyFoodScreen extends PureComponent {
    render() {
        return (
            <View style={styles.container}>
                <Header
                    iconLeft={'menu'}
                    iconRight={'shopping-cart'}
                />
                <HeaderListCompanyFood />
                <ListCompanyFood
                    data={this.props.getDataListFoodCompany}
                    loadMoreData={this.loadMoreDataListFoodCompany}
                    getIsLoaddingListFoodCompany={this.props.getIsLoaddingListFoodCompany}
                    gotoDetail={this.gotoDetailFood}
                />
                <BottomBar
                    title={'KITCHEN'}
                    data={'Italian, Japanese, Healthy food, Georgian, Fuck food'}
                />
            </View >

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f1f1f1'
    },

});

export default ListCompanyFoodScreen;
