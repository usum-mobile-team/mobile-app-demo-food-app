import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '@src/actions';
import * as selectors from '@src/selectors';
import ListDetailFoodOfCompany from "./ListDetailFoodOfCompany";
import { Animated } from "react-native";

class ListDetailFoodOfCompanyContainer extends ListDetailFoodOfCompany {
    constructor(props) {
        super(props);
        this.state = {
            scrollY: new Animated.Value(0)
        }
        this.page = 0;
    }
    loadMoreData = () => {
        //here had enought data for list detail from prev scene, but i wanna do follow take data like data from server
        if (this.page == 1) return;
        this.props.takeDataDetailFoodCompanyRequest(this.props.navigation.getParam('data').id);
        this.page++;
    }
    componentWillUnmount() {
        this.props.resetTakeDataDetailFoodCompany();
    }
    componentDidMount() {
        this.loadMoreData();
    }
}

const mapStateToProps = (state) => {
    return {
        getDataListDetailFoodCompany: selectors.getDataListDetailFoodCompany(state),
        getIsLoaddingListDetailFoodCompany: selectors.getIsLoaddingListDetailFoodCompany(state),
    };
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(ListDetailFoodOfCompanyContainer);
