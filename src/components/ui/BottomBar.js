import React, { PureComponent } from "react";
import { View, StyleSheet, Text } from "react-native";
import * as utils from '@src/utils'
import Icon from "react-native-vector-icons/Feather";

class BottomBar extends PureComponent {
    render() {
        var { title, data } = this.props;
        return (
            <View style={{
                ...styles.container
            }}>
                <Icon
                    name={'menu'}
                    size={utils.scale(40)}
                    color={'red'}
                />
                <Text style={styles.txtTitle}>{title}</Text>
                <Text numberOfLines={1} style={styles.txtData}>{data}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flexDirection: 'row',
        padding: utils.scale(25),
        backgroundColor: 'white',
        borderTopWidth: utils.scale(2),
        borderTopColor: '#d2d2d2'
    },
    txtTitle: {
        color: 'red',
        fontSize: utils.scale(24),
        marginHorizontal: utils.scale(15)
    },
    txtData: {
        color: '#666666',
        fontSize: utils.scale(28),
        flex: 1
    }
});

export default BottomBar;
