import React, { PureComponent } from "react";
import { StyleSheet, Animated, Easing } from "react-native";
import * as utils from '@src/utils';

class AnimationItemList extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            timeAni: new Animated.Value(0)
        }
    }
    componentDidMount() {
        Animated.timing(
            this.state.timeAni,
            {
                toValue: 1,
                duration: 400,
                delay: this.props.index * 300,
                easing: Easing.ease
            }
        ).start();
    }
    render() {
        const marginTop = this.state.timeAni.interpolate({
            inputRange: [0, 1],
            outputRange: [-utils.scale(150), 0]
        });
        const rotate = this.state.timeAni.interpolate({
            inputRange: [0, 1],
            outputRange: ['35deg', '0deg']
        });
        return (
            <Animated.View style={{
                opacity: this.state.timeAni,
                marginTop,
                transform: [{ rotate }, { rotateY: rotate }]
            }}>
                {this.props.children}
            </Animated.View>
        );
    }
}

const styles = StyleSheet.create({

});

export default AnimationItemList;
