import React, { PureComponent } from "react";
import { View, StyleSheet, Text, TouchableOpacity, Animated, TextInput, Keyboard } from "react-native";
import * as utils from '@src/utils'
import Icon from "react-native-vector-icons/Feather";
class BodyHome extends PureComponent {
    render() {
        var { marginTopForInput, marginRightInput, marginLeftButton, showKeyboard,
            inputDisAction, bottomIcon } = this.props;
        return (
            <View style={{ ...styles.container }}>
                <Animated.View style={{
                    ...styles.center,
                    marginRight: marginRightInput,
                    marginTop: marginTopForInput
                }}>
                    <TouchableOpacity style={{ ...styles.btnChooseNational }}>
                        <Text style={{ ...styles.txtChooseNational }}>London</Text>
                        <Icon name='chevron-down' color='#555555' size={utils.scale(35)} />
                    </TouchableOpacity>
                    <Animated.View style={{ ...styles.iconMapPin, top: bottomIcon }}>
                        <Icon name='map-pin' color='white' size={utils.scale(40)} />
                    </Animated.View>
                </Animated.View>
                <Animated.View style={{
                    ...styles.center,
                    marginRight: showKeyboard || inputDisAction ? 0 : marginRightInput,
                }}>
                    <TextInput
                        style={{
                            ...styles.input,
                            borderTopLeftRadius: showKeyboard ? utils.scale(10) : 0,
                            borderTopRightRadius: showKeyboard ? utils.scale(10) : 0,
                            borderWidth: showKeyboard ? 1 : 0,
                        }}
                        placeholder={'Your address'}
                        onChangeText={this.props.changeAddress}
                        onFocus={this.props.onFocusInput}
                    />
                </Animated.View>
                <Animated.View style={{
                    marginLeft: marginLeftButton
                }}>
                    <TouchableOpacity style={{ ...styles.btnSearchFood }}>
                        <Text style={{ ...styles.txtSearchFood }}>Search food</Text>
                    </TouchableOpacity>
                </Animated.View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        marginHorizontal: utils.scale(20),
    },
    input: {
        height: utils.scale(100),
        backgroundColor: 'white',
        width: utils.deviceWidth - utils.scale(80),
        paddingVertical: 0,
        paddingHorizontal: utils.scale(20),
        backgroundColor: 'white',
        borderBottomLeftRadius: utils.scale(10),
        borderBottomRightRadius: utils.scale(10),
        borderColor: 'red'
    },
    btnChooseNational: {
        height: utils.scale(100),
        width: utils.deviceWidth - utils.scale(80),
        borderBottomWidth: utils.scale(3),
        borderBottomColor: '#e2e2e2',
        paddingHorizontal: utils.scale(20),
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        borderTopLeftRadius: utils.scale(10),
        borderTopRightRadius: utils.scale(10)
    },
    txtChooseNational: {
        fontSize: utils.scale(30),
        color: '#555555',
        marginRight: utils.scale(10),
    },
    iconMapPin: {
        height: utils.scale(65),
        width: utils.scale(65),
        borderRadius: utils.scale(65),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'red',
        position: 'absolute',
    },
    btnSearchFood: {
        height: utils.scale(100),
        width: utils.deviceWidth - utils.scale(80),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F39C2D',
        borderRadius: utils.scale(10),
        marginTop: utils.scale(20)
    },
    txtSearchFood: {
        fontSize: utils.scale(32),
        color: 'white',
        fontWeight: 'bold'
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default BodyHome;
