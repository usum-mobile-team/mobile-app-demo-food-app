import React, { PureComponent } from "react";
import { View, StyleSheet, Text, Animated, FlatList, Image, TouchableOpacity } from "react-native";
import * as utils from '@src/utils'
HEADER_MAX_HEIGHT = utils.scale(400)
HEADER_MIN_HEIGHT = 0
HEADER_TITLE_HEIGHT = utils.scale(200)
import Icon from "react-native-vector-icons/Feather";
import AnimationItemList from '@src/components/ui/AnimationItemList'

class ItemListDetailFoodOfCompany extends PureComponent {
    render() {
        const { item, lengthData, index } = this.props;
        return (
            <AnimationItemList index={index}>
                <View style={{
                    ...styles.container,
                    marginBottom: parseInt(index) == lengthData - 1 ? utils.scale(40) : 0
                }}>
                    <Image
                        source={{ uri: item.imageFood }}
                        resizeMode='contain'
                        style={styles.image}
                    />
                    <View style={styles.containInfoTextFood}>
                        <Text style={styles.txtNameFood}>{item.nameFood}</Text>
                        <Text style={styles.txtDescription}>{item.description}</Text>
                    </View>
                    <TouchableOpacity style={styles.containCost}>
                        <Text style={styles.txtMoney}>${item.cost}</Text>
                        <Icon
                            name={'shopping-cart'}
                            size={utils.scale(36)}
                            color={'red'}
                        />
                    </TouchableOpacity>
                </View>
            </AnimationItemList>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        padding: utils.scale(15),
        alignItems: 'center',
        flexDirection: 'row',
        borderBottomColor: '#f3f3f3',
        borderBottomWidth: utils.scale(3),
    },
    image: {
        height: utils.scale(140),
        width: utils.scale(140),
        borderWidth: utils.scale(2),
        borderColor: '#d2d2d2',
        marginHorizontal: utils.scale(10)
    },
    containInfoTextFood: {
        flex: 1,
        marginHorizontal: utils.scale(10)
    },
    txtNameFood: {
        color: '#444444',
        fontSize: utils.scale(28)
    },
    txtDescription: {
        color: '#d2d2d2',
        fontSize: utils.scale(24)
    },
    containCost: {
        paddingHorizontal: utils.scale(30),
        justifyContent: 'center',
        alignItems: 'center'
    },
    txtMoney: {
        fontSize: utils.scale(36),
        color: 'red'
    }
});

export default ItemListDetailFoodOfCompany;
