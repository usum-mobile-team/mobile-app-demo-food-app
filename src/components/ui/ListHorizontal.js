import React, { PureComponent } from 'react';
import { View, Text, ActivityIndicator, ListView, StyleSheet, Image } from 'react-native'
import * as utils from '@src/utils'
import Icon from "react-native-vector-icons/Ionicons";
import StarRate from "./StarRate";
class ListHorizontal extends PureComponent {
    constructor(props) {
        super(props);
        this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.renderRow = this.renderRow.bind(this);
    }

    renderRow(data, sectionId, index) {
        return (
            <View style={{
                margin: utils.scale(10),
                marginLeft: parseInt(index) == 0 ? utils.scale(40) : utils.scale(10),
                marginRight: parseInt(index) == this.props.data.length - 1 ? utils.scale(40) : utils.scale(10),
            }}>
                <View style={{ ...styles.containImage }}>
                    <Image
                        style={{ ...styles.imageCompany }}
                        resizeMode='contain'
                        source={{ uri: data.imageCompany }} />
                </View>
                <Text style={{ ...styles.txtNameCompany }}>{data.nameCompany}</Text>
                <StarRate rate={data.rate} />
            </View>
        )
    }
    render() {
        var dataSource = this.ds.cloneWithRows(this.props.data);
        return (
            <ListView
                style={{ marginVertical: utils.scale(20) }}
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                enableEmptySections={true}
                dataSource={dataSource}
                renderRow={this.renderRow}
            />
        );
    }

}
const styles = StyleSheet.create({
    containImage: {
        marginBottom: utils.scale(10),
        backgroundColor: 'white',
        padding: utils.scale(5),
        borderRadius: utils.scale(10)
    },
    imageCompany: {
        height: utils.scale(170),
        width: utils.scale(170)
    },
    txtNameCompany: {
        fontSize: utils.scale(26),
        color: '#444444'
    }
})
export default ListHorizontal;
