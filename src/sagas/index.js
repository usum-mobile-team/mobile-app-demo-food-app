import { fork } from 'redux-saga/effects';
import { loadInitialState } from './loadInitialState.saga';
import { watchFindLocationFoodCompany, watchListLocationFoodCompany } from './findLocationFoodCompany.saga';
import { watchTakeDataDetailFoodCompany } from './takeDataDetailFoodCompany.saga';

function* rootSagas() {
    yield [
        fork(loadInitialState),
        fork(watchFindLocationFoodCompany),
        fork(watchListLocationFoodCompany),
        fork(watchTakeDataDetailFoodCompany)
    ];
}

export default rootSagas;
