import * as type from '../actionTypes';
import * as utils from '../utils';
const initialState = {
    isLoaddingListFoodCompanySearchByname: false,
    dataListFoodCompanySearchByname: [],
    isLoaddingListFoodCompany: false,
    dataListFoodCompany: [],
};

export default function findLocationFoodCompany(state = initialState, action) {
    switch (action.type) {
        case type.FIND_LOCATION_FOOD_COMPANY_REQUEST:
            return {
                ...state,
                isLoaddingListFoodCompanySearchByname: true
            };
        case type.FIND_LOCATION_FOOD_COMPANY_SUCCESS:
            return {
                ...state,
                isLoaddingListFoodCompanySearchByname: false,
                dataListFoodCompanySearchByname: action.result,
            };
        case type.LIST_LOCATION_FOOD_COMPANY_REQUEST:
            return {
                ...state,
                isLoaddingListFoodCompany: true
            };
        case type.LIST_LOCATION_FOOD_COMPANY_SUCCESS:
            return {
                ...state,
                isLoaddingListFoodCompany: false,
                dataListFoodCompany: action.result,
            };
        case type.RESET_LIST_LOCATION_FOOD_COMPANY:
            return {
                ...state,
                isLoaddingListFoodCompany: false,
                dataListFoodCompany: [],
            };
        default:
            return state;
    }
}
