import * as type from '../actionTypes';

export const loadInitialStateRequest = () => ({
    type: type.LOAD_INITIAL_STATE_REQUEST
});
export const loadInitialStateSuccess = (result) => ({
    type: type.LOAD_INITIAL_STATE_SUCCESS,
    result
});
export const loadInitialStateFailed = (error) => ({
    type: type.LOAD_INITIAL_STATE_FAILED,
    error
});

